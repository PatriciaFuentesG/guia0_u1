#include <iostream>
#include "aminoacido.h"
#include <list>
using namespace std;

#ifndef CADENA_H
#define CADENA_H

class cadena {
    private:
        string letra = "\0";
        list<aminoacido> aminoacidos;

    public:
       
        cadena (string letra);

        string get_letra();
        list<aminoacido> get_aminoacido();
      
        void set_letra(string letra);
        void add_aminoacidos(aminoacido Aminoacidos);
 
};
#endif