/*
 * g++ atomo.cpp -o atomo 
 * 
 */

 
#include <iostream>
using namespace std;

#include "coordenada.h"
#include "atomo.h"


atomo::atomo(string nombre, int numero) {
    this->nombre = nombre;
    this->numero = numero;
    this->Coordenada = coordenada();
}

string atomo::get_nombre() {
    return this->nombre;
}

int atomo::get_numero() {
    return this->numero;
}
        
coordenada atomo::get_coordenas() {
    return this->Coordenada;
}

void atomo ::set_nombre(string nombre) {
    this->nombre = nombre;
}
        
void atomo ::set_numero(int numero){
    this->numero = numero;
}
        
void atomo::set_coordenada(coordenada Coordenadas) {
    this->Coordenada;
}
