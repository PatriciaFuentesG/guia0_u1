prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = programa.cpp proteina.cpp coordenada.cpp cadena.cpp atomo.cpp aminoacido.cpp
OBJ = programa.o proteina.o coordenada.o cadena.o atomo.o aminoacido.o
APP = programa

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
