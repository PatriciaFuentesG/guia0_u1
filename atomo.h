#include <iostream>
#include "coordenada.h"
using namespace std;

#ifndef ATOMO_H
#define ATOMO_H


class atomo {
    private:
        string nombre = "\0";
        int numero = 0;
        coordenada Coordenada;

    public:
       
        atomo(string nombre, int numero);
        
        coordenada get_coordenas();
        string get_nombre();
        int get_numero();
      
        void set_nombre(string nombre);
        void set_numero(int numero);
        void set_coordenada(coordenada Coordenadas);
       
 
};
#endif