#include <iostream>
using namespace std;

#ifndef COORDENADA_H
#define COORDENADA_H

class coordenada {
    private:
        double x = 0;
        double y = 0;
        double z = 0;

    public:
        coordenada();
        coordenada(double x, double y, double z);
        
        double get_x();
        double get_y();
        double get_z();
      
        void set_x(double x);
        void set_y(double y);
        void set_z(double z);
        
};
#endif