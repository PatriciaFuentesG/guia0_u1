#include <iostream>
#include <list>
#include"cadena.h"
using namespace std;

#ifndef PROTEINA_H
#define PROTEINA_H

class proteina {
    private:
        string nombre = "\0";
        string id = "\0";
        list<cadena> Cadenas;

    public:
        
        proteina (string nombre, string );
        
        list<cadena> get_cadenas();
        string get_nombre();
        string get_id();
      
        void set_nombre(string nombre);
        void set_id(string id);
        void add_cadenas(cadena Cadenas);
 
};
#endif