#include <iostream>
#include "atomo.h"
#include <list>
using namespace std;

#ifndef AMINOACIDO_H
#define AMINOACIDO_H

class aminoacido {
    private:
        string nombre = "\0";
        int numero = 0;
        list<atomo> Atomos;

    public:
       
        aminoacido(string nombre, int numero);
        
        list<atomo> get_atomos();
        string get_nombre();
        int get_numero();
      
        void set_nombre(string nombre);
        void set_numero(int numero);
        void add_atomos(atomo Atomo);
 
};
#endif