/*
  * g++ programa.cpp proteina.cpp -o programa
 */
#include <iostream>
#include <list>
#include "proteina.h" 
#include "coordenada.h"
#include "atomo.h"
#include "cadena.h"
#include "atomo.h"
#include "aminoacido.h"
 list<proteina>proteinas;
//Función que toma datos para la confección del objeto proteina
void leer_datos_proteinas() {
   
    //Se declaran las variables que contendrán los datos para los constructores
    string nombre, id, coordenada_x, coordenada_y, coordenada_z, letra;
    string cadenas, aminoaciods, atomos;
    //Se le pide al usuario el nombre y el ID para la proteina
    cout << "Ingrese el nombre de la proteina" << endl;
    getline(cin, nombre);
    cout << "Ingrese el ID de la proteina" << endl;
    getline(cin, id);
    //Se construlle proteina
    proteina Proteina = proteina(nombre, id);
    cout << "¿Cuantas cadenas posee la proteina?" << endl;
    //Se pide al usuario la cantidad de cadenas a agregar a la proteina
    getline(cin, cadenas);
    //Se itera entre la cantidad de cadenas para agregar aminoacidos
    for (int i = 0; i < stoi(cadenas); i++) {
        //se pide la letra de la cadena
        cout << "Ingrese la letra de la cadena" << endl;
        getline(cin, letra);
        cadena Cadena = cadena(letra);
        cout << "Ingrese la cantidad de aminoacidos de la cadena" << endl;
        getline(cin, aminoaciods);
        //se pide la cantidad de aminoacidos a agrega a la cadena y se itera 
        for (int j = 0; j < stoi(aminoaciods); j++) {
            /*Se piden los datos del constructor del aminoacido al igual que 
            la cantidad de atomos de este, para luego pedirlos en ciclo*/
            cout << "Ingrese el nombre del aminoacido numero " << j + 1 << endl;
            getline(cin, nombre);
            aminoacido Aminoacido = aminoacido(nombre, j + 1);
            cout << "Ingrese la cantidad de atomos del aminoacido" << nombre << endl;
            getline(cin, atomos);
            //Finalmente en el ciclo de atomos se ingresan las coordenadas de los mismos
            for (int k = 0; k < stoi(atomos); k++) {
                cout << "Ingrese el nombre del atomo número " << k + 1 << endl;
                getline(cin, nombre);
                atomo Atomo = atomo(nombre, k + 1);
                cout << "Ingrese la coordenada x del atomo" << endl;
                getline(cin, coordenada_x);
                cout << "Ingrese la coordenada y del atomo" << endl;
                getline(cin, coordenada_y);
                cout << "Ingrese la coordenada z del atomo" << endl;
                getline(cin, coordenada_z);
                coordenada coor = coordenada(stof(coordenada_x), stof(coordenada_y), stof(coordenada_z));
                /*ya construidos los objetos con sus datos correspondientes 
                se procede a crear la proteina asociando los objetos creados a sus listas correspondientes*/
                Atomo.set_coordenada(coor);
                Aminoacido.add_atomos(Atomo);
            }
            Cadena.add_aminoacidos(Aminoacido);
        }
        Proteina.add_cadenas(Cadena);
    }
    proteinas.push_back(Proteina);
}
//Función que imprime la totalidad de los datos guardados en la lista proteinas
void imprimir_datos_proteina() {
    system("clear");
    cout << "La/as proteina/as ingresada/as es/son:" << endl;
    //recorre la lista proteinas e impime en pantalla sus valores
    for (proteina i: proteinas) {
        cout << "Nombre proteina: " << i.get_nombre() << endl;
        cout << "ID: " << i.get_id() << endl;
        //luego recupera los datos de la cadena e imprime sus valores
        for (cadena c: i.get_cadenas()) {
            cout << "Cadena " << c.get_letra() << ":" << endl;
            //de cadena se recuperan de manera iterativa los datos de aminoacido
            for (aminoacido a: c.get_aminoacido()) {
                cout << "Aminoacido: " << a.get_numero() << "-" << a.get_nombre() << ":" << endl;
                //de igual manera se recorre la lista atomo y se muestran sus atributos
                for (atomo at: a.get_atomos()) {
                    cout << "" << at.get_nombre() << at.get_numero() << endl;
                    cout << "Coordenadas: ";
                    cout << "(" << at.get_coordenas().get_x() << ", ";
                    cout << at.get_coordenas().get_y() << ", ";
                    cout << at.get_coordenas().get_z() << ")" << endl;
                }
            }
        }
        cout << endl;
    }
    cout << endl;
}

int main(int argc, char * argv[]) {
    //booleano para el ciclo principal del programa
    bool on = true;
    //Variable de selección para el menú
    string opcion;
    cout << "Bienvenido al sistema rudimentario de base de datos proteicos" << endl;
    //Ciclo principal del programa
    while (on) {
        /* se da a escoger entre 3 opciones, las primeras dos son lo que pedía la guia, 
        la tercera cierra el programa*/
        cout << "¿Que es lo que desea hacer?" << endl;
        cout << "1-. Agregar proteína" << endl;
        cout << "2-. Imprimir proteínas" << endl;
        cout << "3-. Salir del programa" << endl;
        getline(cin, opcion);
        //Estrucctura switch llama a las 2 funciones principales del programa
        switch (stoi(opcion)) {
        case 1:
            leer_datos_proteinas();
            break;
        case 2:
            imprimir_datos_proteina();
            break;
        case 3:
      
            on = false;
            break;
        default:
            cout << "Ingrese una selección valida del menú por favor" << endl;
            break;
        }
    }
    return 0;
}
